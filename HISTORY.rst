=======
History
=======

2016.096 (2016-04-05)
------------------
* The fix is in for pySerial 3.0.1, but I don't think the package is
  working. It won't install on WinXP, even though it's supposed to be
  for 32-bit, and it just doesn't seem to want to connect to anything
  when installed on Win7. So just don't use it. The Texan laptops will
  be sticking with vesion 2.7 until this gets sorted out. It's been
  fine for years.

2016.119 (2016-04-28)
------------------
* Changed the Check-Out/Check-In function to handle Texan barcodes
  that are 7-digits, but with a leading letter.

2016.267 (2018-06-07)
------------------
* First release on new build system.
* LastDiff column in time checks was not going yellow when the
  difference was >10ms. Now it does yellow and red when the difference
  is 8ms and 24ms like everywhere else.

2019.065 (2019-02-25)
------------------
* Should be good under Python 2 or 3.
* A lot of little messages bugs fixed that no one noticed.
* The menu item to change font sizes is now Change Font Sizes, instead
  of Fonts BIGGER and Fonts smaller.
