=====
pocus
=====

For programming, offloading and testing RT-125A dataloggers

* Free software: GNU General Public License v3 (GPLv3)


Features
--------

* Provides communication with the RefTek RT-125A "Texan" dataloggers
  for programming them prior to deployment, offloading recorded data,
  analysis and plotting of data, and troubleshooting equipment
  problems.


Credits
-------

This package was created with Cookiecutter_ and the
`passoft/cookiecutter`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`passoft/cookiecutter`: https://git.passcal.nmt.edu/passoft/cookiecutter
