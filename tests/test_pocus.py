#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `pocus` package."""

import unittest
import sys

try:
    import pocus
except ImportError:
     pass

class TestPocus(unittest.TestCase):
    """Tests for `pocus` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_import(self):
        if 'pocus' in sys.modules:
            self.assert_(True, "pocus loaded")
        else:
            self.fail()

